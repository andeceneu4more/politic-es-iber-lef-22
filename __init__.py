import os
from glob import glob
import pickle

data_folder = 'data'
pictures_folder = 'pictures'
logs_folder = 'logs'

dev_train_path = os.path.join(data_folder, 'development.csv')
dev_test_path = os.path.join(data_folder, 'development_test.csv')
train_train_path = os.path.join(data_folder, 'training.csv')

train_json = os.path.join(data_folder, 'acumulated_train.json')
dev_json = os.path.join(data_folder, 'acumulated_test.json')

train_chunk_json = os.path.join(data_folder, 'chunk_train.json')
dev_chunk_json = os.path.join(data_folder, 'chunk_test.json')

extremes_path = os.path.join(data_folder, 'extremes.json')
atributed_vocab_path = os.path.join(data_folder, 'atributed_vocab.txt')
political_vocab_path = os.path.join(data_folder, 'extremes_vocab.txt')

ner_path = os.path.join(data_folder, "ner.csv")
custom_tokenzier_path = os.path.join(data_folder, 'custom_tokenizer')
custom_model_path = os.path.join(data_folder, 'custom_model')
multi_labels_weights_path = os.path.join(data_folder, 'multi_labels_class_weights.npy')
vectorized_data_path = os.path.join(data_folder, "vectorized_data.npy")
vectorizer_path = os.path.join(data_folder, "vectorizer.npy")

tweeter_keywords = ["political_party", "@user"]

def get_new_experiment_folder(model_name='dccuchile/bert-base-spanish'):
    folder_re = os.path.join(logs_folder, f'{model_name}*')
    experiment_id = len(glob(folder_re))
    experiment_folder = os.path.join(logs_folder, f'{model_name}_{experiment_id}')
    os.makedirs(experiment_folder)
    return experiment_folder

def pickle_load(exp_dir):
    model_path = os.path.join(exp_dir, 'best.pkl')
    with open(model_path, 'rb') as fin:
        model = pickle.load(fin)
    return model