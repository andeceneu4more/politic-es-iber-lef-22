from torch.utils.data import Dataset
from transformers import AutoTokenizer
from __init__ import train_json, dev_json, custom_tokenzier_path

import json
import torch
import pdb
import pandas as pd
import numpy as np

class SpanishDataset(Dataset):
    def __init__(self, scope, column_target='ideology_binary_enc', model_name='CenIA/albert-tiny-spanish'):
        if scope == "train":
            json_path = train_json
        elif scope == "dev":
            json_path = dev_json
        
        self.tokenizer = AutoTokenizer.from_pretrained(model_name)
        self.max_len = 160
        self.label_name = column_target
        with open(json_path, 'r', encoding='utf8') as fin:
            self.json_list = json.load(fin)

    def __len__(self):
        return len(self.json_list)

    def __getitem__(self, index):
        entry = self.json_list[index]
        text = entry["clean_tweet"]
        input_dict = self.tokenizer(text, return_tensors='pt', truncation=True, \
                                            padding='max_length', max_length=self.max_len)
        input_ids = input_dict['input_ids'][0]
        attention_mask = input_dict['attention_mask'][0]
        label = entry[self.label_name]
        return input_ids, attention_mask, torch.tensor(label)

