from __init__ import logs_folder, vectorizer_path, pickle_load
# from train_test_light import pickle_load, get_vectorized_data
from sklearn.inspection import permutation_importance

import os
import json
import pdb
import pickle
import numpy as np
import matplotlib.pyplot as plt

def plot_binary_classification_importance(label, word_names, features_importance, indices_, exp_dir):
    plt.figure(figsize=(20 * len(indices_) / 10, 7))

    words = [word_names[idx] if idx < len(word_names) else 'OOF' for idx in indices_]
    plt.bar(words, features_importance[indices_])
    plt.title(f"Importance for label {label}")
    
    figure_path = os.path.join(exp_dir, f"{label}_keywords.png")
    plt.savefig(figure_path)


# config_path = os.path.join(exp_dir, "config.json")
# with open(config_path, 'r') as fin:
#     config = json.load(fin)

# train_texts_vec, dev_texts_vec, train_labels, dev_labels = get_vectorized_data("ideology_binary_enc")

# # dense_train_vec = [train_vec.toarray()[0] for train_vec in train_texts_vec]
# dense_dev_vec = [dev_vec.toarray()[0] for dev_vec in dev_texts_vec]
# result = permutation_importance(model, dense_dev_vec, dev_labels, n_repeats=2, random_state=101, n_jobs=4)


def random_forest_importances(word_names, exp_dir, topk):
    model = pickle_load(exp_dir)
    features_importance = model.feature_importances_

    sorted_indices = np.argsort(features_importance)
    indices_ = sorted_indices[-topk:]

    plot_binary_classification_importance("model", word_names, features_importance, indices_, exp_dir)


def plot_svm_binary_classification_viz(exp_dir, model, topk=50):
    with open(vectorizer_path, 'rb') as fin:
        vectorizer = pickle.load(fin)

    word_names = vectorizer.get_feature_names()
    word_names = np.array(word_names)
    
    features_importance = model.coef_.toarray()[0]
    sorted_indices = np.argsort(features_importance)

    worst_indices = sorted_indices[:topk]
    best_indices = sorted_indices[-topk:]

    plot_binary_classification_importance(0, word_names, features_importance, worst_indices, exp_dir)
    plot_binary_classification_importance(1, word_names, features_importance, best_indices, exp_dir)


def main(topk = 50):
    exp_dir = os.path.join(logs_folder, 'RandomForestClassifier_3')
    config_path = os.path.join(exp_dir, "config.json")
    with open(config_path, 'r') as fin:
        config = json.load(fin)
    
    with open(vectorizer_path, "rb") as fin:
        vectorizer = pickle.load(fin)

    word_names = vectorizer.get_feature_names()
    word_names = np.array(word_names)

    if config["model_name"] == "RandomForestClassifier":
        random_forest_importances(word_names, exp_dir, topk)

if __name__ == "__main__":
    main()