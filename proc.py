from audioop import reverse
from __init__ import data_folder, dev_train_path, train_train_path, ner_path, multi_labels_weights_path
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import train_test_split
from transformers import BertTokenizer
from entities import named_mappings

from sklearn.utils.class_weight import compute_class_weight

import pandas as pd
import numpy as np
import string
import re
import os
import emoji
import json
import spacy
from legibilidad.legibilidad import gutierrez

emoji_re = emoji.get_emoji_regexp()
nlp = spacy.load("es_core_news_sm")
tokenizer = BertTokenizer.from_pretrained('dccuchile/bert-base-spanish-wwm-uncased')
tokens_max_len = 160
import pdb

label_names = ['ideology_binary_enc', 'ideology_multiclass_enc', 'ideology_middle_enc', 'profession_enc', 'gender_enc']

def encode_labels(df, label_name):
    ideology_ = df[label_name].tolist()
    encoder = LabelEncoder()
    ideology_enc = encoder.fit_transform(ideology_)
    df[f'{label_name}_enc'] = ideology_enc

    pair_list = list(enumerate(encoder.classes_.tolist()))
    print(f"Encoder for {label_name} finished with {pair_list}")
    return df

def explain_hashtags(tweet):
    hashtags = re.findall(r'#\w+', tweet)
    if len(hashtags) > 0:
        for hashtag in hashtags:
            hasword = hashtag[1].upper() + hashtag[2:]

            hashwords = re.findall(r'[A-Z](?:[a-z]+|[A-Z]*(?=[A-Z]|$))', hasword)
            mention = ' '.join(hashwords).lower()
            tweet = tweet.replace(hashtag, mention)
    return tweet

def clear_tweet(tweet):
    for key, value in named_mappings.items():
        tweet = re.sub(key, value, tweet, flags=re.I)
    
    tweet = tweet.replace("[POLITICAL_PARTY]", "partido político")
    tweet = explain_hashtags(tweet)

    clean_tweet = tweet.replace('“', ' ')\
                       .replace('”', ' ')\
                       .replace('«', ' ')\
                       .replace('»', ' ')\
                       .replace('‘', '\'')\
                       .replace('’', '\'')\
                       .replace('⁃', ' ')\
                       .replace('#', ' ')\
                       .replace('@', ' ')\
                       .replace('. . ', '. ')\
                       .replace('⁦', ' ')\
                       .replace('⁩', ' ')
    clean_tweet = re.sub(emoji_re, ' ', clean_tweet)
    clean_tweet = re.sub(r"\*+", " ", clean_tweet)

    clean_tweet = re.sub(r'\?+', '?', clean_tweet)
    clean_tweet = re.sub(r'!+', '!', clean_tweet)
    clean_tweet = re.sub(r'\.+', '.', clean_tweet)

    punctuations = re.escape(''.join('"$%&\'()*+-/:<=>@[\\]^`{|}~'))
    clean_tweet = re.sub(rf'[{punctuations}]', ' ', clean_tweet)
    clean_tweet = re.sub(r'\s+', ' ', clean_tweet) 

    return clean_tweet


def get_splited_sents(doc, sents_tokens):
    splited_sents = []
    current_sent = ''
    current_len = len(current_sent)
    for sent, sent_tokens in zip(doc.sents, sents_tokens):
        if current_len + len(sent_tokens) > tokens_max_len:
            splited_sents.append(current_sent)
            current_sent = ''
        
        current_sent += sent.text + ' '
        current_len = len(current_sent)
    
    if current_sent != '':
        splited_sents.append(current_sent)
    return splited_sents


named_entities = []

def get_clear_subtweets(tweet):
    clean_tweet = clear_tweet(tweet)
    doc = nlp(clean_tweet)
    
    # for ent in doc.ents:
    #     if '¿' not in ent.text:
    #         named_entities.append([ent.text, ent.label_])
    # tokenized_ = tokenizer.tokenize(clean_tweet)
    # if tokenizer.unk_token in tokenized_:
    #     pdb.set_trace()

    tweet_real_len = 0
    sent_tokens = []
    for sent in doc.sents:
        these_tokens = tokenizer.tokenize(sent.text)
        tweet_real_len += len(these_tokens)
        sent_tokens.append(these_tokens)
    
    clean_list = [clean_tweet] \
                 if tweet_real_len <= tokens_max_len \
                 else get_splited_sents(doc, sent_tokens)
    return clean_list

def save_df_as_json(basename, suffix, df):
    filename = os.path.join(data_folder, f'{basename}_{suffix}.json')

    json_str = df.to_json(orient='records')
    json_ = json.loads(json_str)
    with open(filename, 'w', encoding='utf8') as fout:
        json.dump(json_, fout, indent=4, sort_keys=True, ensure_ascii=False)


def save_as_split_json_or_not(df, csv_path, prefix, to_split):
    if to_split:
        X_train_idx, X_test_idx, _, _= train_test_split(np.arange(len(df)), np.zeros(len(df)), \
                                                        test_size=0.2, random_state=101)
        df_train = df.loc[X_train_idx]

        df_train["legibilidad"] = [gutierrez(tweet) for tweet in df_train["clean_tweet"].tolist()]  
        df_train = df_train.sort_values(by='legibilidad', ascending=False)
        
        save_df_as_json(prefix, 'train', df_train)

        df_test = df.loc[X_test_idx]
        save_df_as_json(prefix, 'test', df_test)
    else:
        save_df_as_json(csv_path, '', df)

def export_class_weights(class_weigths_path, labels):
    class_weights = compute_class_weight("balanced", classes=list(range(max(labels) + 1)), y=labels)
    np.save(class_weigths_path, class_weights)

def main_cumulated(df, csv_path, to_split):
    grouped_df = df.groupby("label")
    user_ids = grouped_df.groups.keys()

    clean_tweets = []
    clean_labels = []
    for i, user_id in enumerate(user_ids):
        user_data = grouped_df.get_group(user_id)

        full_tweet = '. '.join(user_data['tweet'].to_list())
        clean_subtweets = get_clear_subtweets(full_tweet)
        clean_tweets += clean_subtweets

        current_label = user_data[label_names].iloc[0].tolist()
        clean_labels += [current_label for _ in range(len(clean_subtweets))]
        print(f"Processing users progress {i}/{len(user_ids)}", end='\r')
    print()

    new_df = pd.DataFrame()
    new_df['clean_tweet'] = clean_tweets
    new_df[label_names] = clean_labels

    export_class_weights(multi_labels_weights_path, new_df['ideology_multiclass_enc'].tolist())
    save_as_split_json_or_not(new_df, csv_path, 'acumulated', to_split)

def extract_from_morphem(morphem, prop_re):
    matching = re.search(prop_re, morphem)
    if matching is not None:
        str_ = matching.group()
        property = str_.split('=')[-1]
    else:
        property = 'unk'
    return property

def extract_linguistic_signature(morphem):
    person_ = extract_from_morphem(morphem, "person=\d+")  
    number_ = extract_from_morphem(morphem, "number=\w+")
    gender_ = extract_from_morphem(morphem, "gender=\w+")
    return f'{gender_}:{number_}:{person_}'

def main_chunk(df, csv_path, to_split):
    clean_tweets = []
    ner_free_tweets = []
    signature_tweets = []
    for i, tweet in enumerate(df['tweet'].tolist()):
        clean_tweet = clear_tweet(tweet)
        clean_tweets.append(clean_tweet)
        
        doc = nlp(clean_tweet)
        
        ner_free = []
        signature_ = []
        for token in doc:
            if token.pos_ != 'PROPN':
               ner_free.append(token.text)
            else:
                if len(ner_free) == 0 or ner_free[-1] != 'entidad':
                    ner_free.append('entidad')
            
            morphem = str(token.morph).lower()
            if "gender" in morphem or 'person' in morphem or 'number' in morphem:
                signature = extract_linguistic_signature(morphem)
                signature_.append(f'{token.pos_}:{signature}')

        ner_free_tweet = ' '.join(ner_free)
        ner_free_tweets.append(ner_free_tweet)

        signature_tweet = ' '.join(signature_)
        signature_tweets.append(signature_tweet)

        print(f"Processing users progress {i}/{len(df)}", end='\r')
    print()

    df['clean_tweet'] = clean_tweets
    df['ner_free_tweet'] = ner_free_tweets
    df['signature'] = signature_tweets

    # export_class_weights(multi_labels_weights_path, df['ideology_multiclass_enc'].tolist())
    save_as_split_json_or_not(df, csv_path, 'chunk', to_split)


def main(csv_path, to_split):
    df = pd.read_csv(csv_path)

    df = encode_labels(df, 'ideology_binary')
    df = encode_labels(df, 'ideology_multiclass')
    df = encode_labels(df, 'profession')
    df = encode_labels(df, 'gender')

    # New label if it's middle or not
    df["ideology_middle_enc"] = df['ideology_multiclass_enc'].between(1, 2, inclusive='both').astype(int)
    main_cumulated(df, csv_path, to_split)
    main_chunk(df, csv_path, to_split)

    # named_entities.sort(key=lambda x: x[0])
    # texts, labels = list(zip(*named_entities))
    # ner_df = pd.DataFrame()
    # ner_df["text"] = texts
    # ner_df["label"] = labels
    # ner_df.to_csv(ner_path, index=False)


def merge_dataframes(*paths):
    df_acc = pd.DataFrame()
    for path in paths:
        df = pd.read_csv(path)
        df_acc = df_acc.append(df)
    acc_path = os.path.join(data_folder, 'acumulated.csv')
    df_acc.to_csv(acc_path, index=None)
    return acc_path

if __name__ == "__main__":
    train_df_path = merge_dataframes(train_train_path, dev_train_path)
    main(train_df_path, to_split=True)


# Encoder for ideology_binary finished with [(0, 'left'), (1, 'right')]
# Encoder for ideology_multiclass finished with [(0, 'left'), (1, 'moderate_left'), (2, 'moderate_right'), (3, 'right')]
# Encoder for profession finished with [(0, 'journalist'), (1, 'politician')]
# Encoder for gender finished with [(0, 'female'), (1, 'male')]


# TODO: download the train test, train on it inside kaggle/collab and then interprete it

# TO REASEARCH:
# stance detection from Cornelia Caragea
# Interpreters backend (https://towardsdatascience.com/introducing-transformers-interpret-explainable-ai-for-transformers-890a403a9470)
# https://github.com/cdpierse/transformers-interpret