from models import PoliticalBeto
from datasets import SpanishDataset
from torch.utils.data import DataLoader
from transformers import DistilBertForSequenceClassification, AlbertForSequenceClassification

from torch.optim.lr_scheduler import ReduceLROnPlateau
from torch.utils.tensorboard import SummaryWriter
from torch.optim import AdamW
from __init__ import get_new_experiment_folder, multi_labels_weights_path, custom_model_path

import torch
import os
import torch.nn as nn
import numpy as np
import pdb
import json

device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu') 

def is_internal_parameter(name):
    if name.startswith("distilbert.embeddings"):
        return True
    elif name.startswith("distilbert.transformer.layer"):
        layer_id = int(name.split('.')[3])
        if layer_id < 10:
            return True
    return False

def get_model_by_name(model_name, n_target):
    if model_name.startswith('dccuchile/bert-base-spanish'):
        model = PoliticalBeto.from_pretrained(model_name, num_labels=n_target)
        all_parameters = model.bert.named_parameters()
    elif model_name.startswith('CenIA/distillbert'):
        model = DistilBertForSequenceClassification.from_pretrained(model_name, num_labels=n_target)
        all_parameters = model.distilbert.named_parameters()
    elif model_name.startswith('CenIA/albert'):
        model = AlbertForSequenceClassification.from_pretrained(model_name, num_labels=n_target)
        all_parameters = model.albert.embeddings.named_parameters()
    else:
        model = PoliticalBeto.from_pretrained(model_name, num_labels=n_target)
        all_parameters = model.bert.parameters()

    # for name, parameter in all_parameters:
    #     parameter.requires_grad = False
    return model.to(device)

def get_lr(optimizerr):
    for param_group in optimizerr.param_groups:
        return param_group['lr']

def train_epoch(epoch, model, dataloaders, optimizerr, lr_sched, writer, exp_dir, best_loss, criterion):
    for phase in ['train', 'dev']:
        epoch_losses = []

        if phase == 'train':
            model.train()
        else:
            model.eval()

        for i, batch in enumerate(dataloaders[phase]):
            input_ids, attention_mask, labels = batch
            input_ids = input_ids.to(device)
            attention_mask = attention_mask.to(device)
            labels = labels.to(device)

            optimizerr.zero_grad()
            with torch.set_grad_enabled(phase == 'train'):
                output = model(input_ids, attention_mask=attention_mask)
                output = nn.Softmax(dim=-1)(output[0])
                loss = criterion(output, labels) #(output[0][:, 0], labels)

                if phase == 'train':
                    loss.backward()
                    optimizerr.step()

            epoch_losses.append(loss.item())
            average_loss = np.mean(epoch_losses)
            lr = get_lr(optimizerr)

            if (i + 1) % 10 == 0:
                loading_percentage = int(100 * (i+1) / len(dataloaders[phase]))
                print(f'{phase}ing epoch {epoch}, iter = {i+1}/{len(dataloaders[phase])} ' + \
                    f'({loading_percentage}%), loss = {loss}, average_loss = {average_loss} ' + \
                    f'learning rate = {lr}', end='\r')
                
        if phase == 'dev' and average_loss < best_loss:
            best_loss = average_loss
            
            torch.save({
                    'model': model.state_dict(),
                    'optimizerrizer': optimizerr.state_dict(),
                    'lr_sched': lr_sched.state_dict(),
                    'epoch': epoch,
                    'dev_loss': best_loss
                }, os.path.join(exp_dir, 'best.pth'))
            
        if phase == 'train':
            metric_results = {
                'train_loss': average_loss
            }

            writer.add_scalar('Train/Loss', average_loss, epoch)
            writer.flush()

        if phase == 'dev':
            val_results = {
                'dev_loss': average_loss
            }

            metric_results.update(val_results)
            writer.add_scalar('Dev/Loss', average_loss, epoch)
            writer.flush()

            try:
                lr_sched.step()
            except:
                lr_sched.step(average_loss)
        
        print()
    return best_loss


def main():
    config = {
        "model_name": "CenIA/albert-tiny-spanish",
        "batch_size": 8,
        "optimizer": "AdamW",
        "n_epochs": 10,
        "column_target": "gender_enc",
        "n_target": 2
    }

    if config["column_target"] == "ideology_multiclass_enc":
        class_weighs = np.load(multi_labels_weights_path, allow_pickle=True)
        class_weighs = torch.tensor(class_weighs).float().to(device)
        criterion = nn.CrossEntropyLoss(weight=class_weighs)
    else:
        criterion = nn.CrossEntropyLoss()
    
    batch_size = config["batch_size"]
    train_dataset = SpanishDataset('train', config["column_target"], config['model_name'])
    train_dataloader = DataLoader(train_dataset, batch_size=batch_size, \
                                        shuffle=False, num_workers=4)
    dev_dataset = SpanishDataset('dev', config['column_target'], config['model_name'])
    dev_dataloader = DataLoader(dev_dataset, batch_size=batch_size, \
                                        shuffle=False, num_workers=4)
    dataloaders = {
        "train": train_dataloader,
        "dev": dev_dataloader
    }

    model_name = config.get("model_name")
    model = get_model_by_name(model_name, config["n_target"])

    if config["optimizer"] == "AdamW":
        optimizerr = AdamW(model.parameters())#, lr=5e-5, eps=1e-8)
    lr_sched = ReduceLROnPlateau(optimizerr, factor = 0.1, patience = 3, mode = 'min')

    exp_dir = get_new_experiment_folder(model_name="CenIA/albert-tiny-spanish")
    writer = SummaryWriter(os.path.join(exp_dir, 'runs'))
    config_path = os.path.join(exp_dir, 'config.json')
    with open(config_path, 'w') as fout:
        json.dump(config, fout, indent=4, sort_keys=True)

    best_loss = 5000
    for epoch in range(config["n_epochs"]):
        best_loss = train_epoch(epoch, model, dataloaders, optimizerr, lr_sched, writer, exp_dir, best_loss, criterion)


if __name__ == "__main__":
    main()

# https://huggingface.co/docs/transformers/model_doc/xlnet#overview
# https://towardsdatascience.com/transfer-learning-in-nlp-for-tweet-stance-classification-8ab014da8dde
# https://aclanthology.org/2021.naacl-main.148.pdf