from __init__ import ner_path, custom_tokenzier_path, custom_model_path
from transformers import BertTokenizer

import torch
import torch.nn as nn

from models import PoliticalBeto

# ner_df = pd.read_csv(ner_path)
# values, counts = np.unique(ner_df["text"], return_counts=True)
# sorted_indices = np.argsort(counts)
# sorted_indices[:15]

manual_keywords = [
    "Gobierno", "Congreso", "Estado", "Donaciones", "Comisión Europea", "Los progres", "Iglesias", 
    "Los fascistas", 

    "LGTBI", "Lorca", "Adolfo Suàrez", "Donald Trump", "Pedro Sánchez",
    "Albert Rivera", "Irene Montero", "Merkel", "Susana Díaz"
    
    "Francia", "Afganistan", "Talibanes", "España", "Cataluña", "Canarias", "Andalucía", "Madrid", "UE", "Europa"
    
    'CCAA', 'Ahora', 'Ayuso','Ayer', 
    'partido político', 'user'
]

named_mappings = {
    "C’s": "C_s",
    "el Rey": "el_Rey",
    "Majestad El Rey": "el_Rey",

    "Donald Trump": "D_Trump",
    "Trump": "D_Trump",
    "Biden": "J_Biden",
    "Joe Biden": "J_Biden",
    
    "PedroSanchez": "P_Sanchez",
    "Pedro Sánchez": "P_Sanchez",
    "Sánchez": "P_Sanchez",

    "Adolfo Suàrez": "A_Suarez",
    "Adolfo Suárez": "A_Suarez",


    "1️⃣": "1. ",
    "2️⃣" : "2. ",
    "3️⃣": "3. ", 
    "4️⃣": "4. ",

    "≤": " más bajo o igual ",
    "&gt;": " mayor que ",
    "&lt;": " más bajo que ",
    "ºC": " grados celsius ",
    "€": " euro "
}


def main(model_name = 'dccuchile/bert-base-spanish-wwm-uncased'):
    tokenizer = BertTokenizer.from_pretrained(model_name)
    model = PoliticalBeto.from_pretrained(model_name, num_labels=4)
    
    new_tokens = ["C_s", "el_Rey", "D_Trump", "P_Sanchez", "A_Suarez"]
    tokenizer.add_tokens(new_tokens)

    new_text = "D_Trump con el_Rey, P_Sanchez"
    print(tokenizer.tokenize(new_text))
    print(tokenizer.encode(new_text))
    tokenizer.save_pretrained(custom_tokenzier_path)

    pretrained_embeddings = model.bert.embeddings.word_embeddings.weight.cpu()
    added_embeddings = nn.Embedding(len(new_tokens), pretrained_embeddings.size(1)).weight
    new_embeddings = torch.cat([pretrained_embeddings, added_embeddings], axis=0)
    model.bert.embeddings.word_embeddings = nn.Embedding.from_pretrained(new_embeddings)

    model.config.update({
        "vocab_size": new_embeddings.size(0)
    })
    model.save_pretrained(custom_model_path)


if __name__ == "__main__":
    main()

# 0.745887 (10)	0.737976 (11)	0.719251 (14)	0.902198 (5)	0.624124 (10)
# 0.733122 (10)	0.737976 (10)	0.668188 (14)	0.902198 (5)	0.624124 (10)
# 0.684379 (11)	0.693878 (9)	0.668188 (12)	0.796037 (10)	0.579415 (10)
# 0.581635 (14)	0.629944 (13)	0.432432 (13)	0.702381 (13)	0.561781 (11)