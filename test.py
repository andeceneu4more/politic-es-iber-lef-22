from datasets import SpanishDataset
from torch.utils.data import DataLoader
from sklearn.metrics import confusion_matrix

import os
import json
import torch
import torch.nn as nn
import pdb
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns

from __init__ import dev_json, logs_folder

from train import get_model_by_name, device
from train_test_light import compute_all_metrics, plot_and_save_confusions

from transformers_interpret import SequenceClassificationExplainer

def test_general_metrics(model, dev_dataloader, column_target):
    real_labels = []
    out_labels = []
    out_probas = []
    for i, batch in enumerate(dev_dataloader):
        input_ids, attention_mask, labels = batch
        # input_ids = input_ids.to(device)
        # attention_mask = attention_mask.to(device)
        real_labels += labels.tolist()

        with torch.no_grad():
            output = model(input_ids, attention_mask=attention_mask)
            # if column_target == "ideology_multiclass_enc":
            output = nn.Softmax(dim=-1)(output[0])
            result_labels = torch.argmax(output, axis=-1).cpu()
            # else:
            #     output_ = nn.Sigmoid()(output[0])
            #     result_labels = (output_[:, 0] >= 0.5).int().cpu()
            #     out_probas += output[0][:, 0].cpu().tolist()
        out_labels += result_labels.tolist()

        print(f'Testing progress {i}/{len(dev_dataloader)}', end='\r')

    labels_pair = np.array([real_labels, out_labels])
    probas_pair = np.array([real_labels, out_probas])
    return labels_pair, probas_pair

def plot_attention_layers(experiment_path, dev_dataset, model):
    attention_folder = os.path.join(experiment_path, 'attention')
    if not os.path.exists(attention_folder):
        os.makedirs(attention_folder)

    input_ids, _ = dev_dataset[0]
    input_ids = input_ids.unsqueeze(0).to(device)
    with torch.no_grad():
        _, attns = model(input_ids, output_attentions=True)
        # Remove attention from <CLS> (first) and <SEP> (last) token
        attns = torch.stack([attn[0, :, 1:-1, 1:-1] for attn in attns])
    
    for layer_id in range(attns.size(0)):
        for head_id in range(attns.size(1)):
            token_attn_matrix = attns[layer_id, head_id].cpu().numpy()
            sns.heatmap(token_attn_matrix, cmap='Blues')
            
            heatmap_path = os.path.join(attention_folder, f'heatmap_layer_{layer_id}_head_{head_id}.png')
            plt.savefig(heatmap_path)
            plt.close()
            print(f"Attention progress {layer_id * attns.size(0) + head_id}/{attns.size(0) * attns.size(1)}", end="\r")

def get_words_attribution(model, tokenizer, experiment_path):
    cls_explainer = SequenceClassificationExplainer(model, tokenizer)
    with open(dev_json, 'r', encoding='utf8') as fin:
        json_list = json.load(fin)
    
    attributions = []
    for i, item in enumerate(json_list):
        text = item["clean_tweet"]

        word_attributions = cls_explainer(text)
        attributions.append({
            "attributions_list": [[attribution[0], float(attribution[1])] for attribution in word_attributions],
            "label": item['ideology_binary_enc']
        })
        print(f'Attribution progress {i}/{len(json_list)}', end='\r')
    
    attributions_path = os.path.join(experiment_path, 'attributions.json')
    with open(attributions_path, 'w', encoding='utf8') as fout:
        json.dump(attributions, fout, indent=4, sort_keys=True, ensure_ascii=False)
    return attributions

def main():
    exp_dir = os.path.join(logs_folder, 'CenIA', 'distillbert-base-spanish-uncased_4')
    config_path = os.path.join(exp_dir, 'config.json')
    weights_path = os.path.join(exp_dir, 'best.pth')

    with open(config_path, 'r') as fin:
        config = json.load(fin)

    model = get_model_by_name(config["model_name"], config["n_target"]).cpu()
    state_dict = torch.load(weights_path, map_location='cpu')
    model.load_state_dict(state_dict["model"])
    model.eval()

    dev_dataset = SpanishDataset('dev', config['column_target'], config["model_name"])
    dev_dataloader = DataLoader(dev_dataset, batch_size=8, \
                                shuffle=False, num_workers=1)
    
    labels_pair, probas_pair = test_general_metrics(model, dev_dataloader, config['column_target'])

    dev_labels, pred_labels = labels_pair
    accuracy, f1_weighted, f1_macro = compute_all_metrics(dev_labels, pred_labels)
    config.update({
        "acccuracy": float(accuracy),
        "f1_weighted": float(f1_weighted),
        "f1_macro": float(f1_macro)
    })
    with open(config_path, 'w') as fout:
        json.dump(config, fout, indent=4, sort_keys=True)
    
    conf_matrix = confusion_matrix(dev_labels, pred_labels)
    plot_and_save_confusions(exp_dir, conf_matrix)

    labels_path = os.path.join(exp_dir, 'labels_dev.npy')
    np.save(labels_path, labels_pair)

    probas_path = os.path.join(exp_dir, 'probas_dev.npy')
    np.save(probas_path, probas_pair)


    # tokenizer = BertTokenizer.from_pretrained('dccuchile/bert-base-spanish-wwm-uncased')
    # plot_attention_layers(experiment_path, dev_dataset, model)

    # attributions = get_words_attribution(model, tokenizer, experiment_path)
    # attributions_path = os.path.join(experiment_path, 'attributions.json')
    # with open(attributions_path, 'w', encoding='utf8') as fout:
    #     json.dump(attributions, fout, indent=4, sort_keys=True, ensure_ascii=False)
    # draw_all_words_attributions(experiment_path)




if __name__ == "__main__":
    main()