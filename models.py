from transformers import BertModel

import torch
import torch.nn as nn
import pdb

from transformers.models.bert.modeling_bert import BertForSequenceClassification

class PoliticalBeto(BertForSequenceClassification):
    def __init__(self, config):
        super().__init__(config)
        self.classifier = nn.Sequential(
            nn.Linear(768 * 2 + 160 ** 2, 256),
            nn.Dropout(0.2),
            nn.GELU(),
            nn.Linear(256, config.num_labels)
        )
        self.flatten = nn.Flatten()        

    def forward(
        self,
        input_ids = None,
        attention_mask = None,
        token_type_ids = None,
        position_ids = None,
        head_mask = None,
        inputs_embeds = None,
        labels = None,
        output_attentions = True,
        output_hidden_states = None,
        return_dict = None,
    ):
        
        return_dict = return_dict if return_dict is not None else self.config.use_return_dict

        outputs = self.bert(
            input_ids,
            attention_mask=attention_mask,
            token_type_ids=token_type_ids,
            position_ids=position_ids,
            head_mask=head_mask,
            inputs_embeds=inputs_embeds,
            output_attentions=output_attentions,
            output_hidden_states=output_hidden_states,
            return_dict=return_dict,
        )
        pooled_output = outputs.pooler_output
        avg_output = torch.mean(outputs.last_hidden_state, axis=1)
        max_attn, _ = torch.max(outputs.attentions[-1], axis=1)
        max_attn = self.flatten(max_attn)
        cat_output = torch.cat([pooled_output, avg_output, max_attn], axis=1)
        
        cat_output = self.dropout(cat_output)
        logits = self.classifier(cat_output)
        return [logits]
