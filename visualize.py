import pandas as pd
import json
import pdb
import re
import string

import matplotlib.pyplot as plt
import numpy as np
import spacy
import os

from __init__ import pictures_folder, dev_train_path, extremes_path, political_vocab_path, tweeter_keywords, atributed_vocab_path
from nltk.corpus import stopwords

nlp = spacy.load("es_core_news_sm")
es_stopwords = stopwords.words("spanish")
forbidden_words = ["“", "”", "...", "¿", "¡", ""]

with open(political_vocab_path, "r", encoding='utf8') as fin:
    political_keywords_ = fin.read().split('\n')

def extract_from_morphem(morphem, prop_re):
    matching = re.search(prop_re, morphem)
    if matching is not None:
        str_ = matching.group()
        property = str_.split('=')[-1]
    else:
        property = 'unk'
    return property


def extract_linguistic_signature(morphem):
    person_ = extract_from_morphem(morphem, "person=\d+")    
    number_ = extract_from_morphem(morphem, "number=\w+")
    return f'{person_}:{number_}'

def get_person_signature(tweet):
    person_signature = []
    for token in nlp(tweet):
        morphem = str(token.morph).lower()
        if "person" in morphem:
            signature = extract_linguistic_signature(morphem)
            person_signature.append(f'{token.pos_}:{signature}')
    return person_signature


def get_clean_tweet_words(tweet):
    clean_tokens = []
    for token in nlp(tweet):
        token_str = token.text.lower()
        if token_str.isalpha() and token_str not in forbidden_words and \
           token_str not in string.punctuation and token_str not in es_stopwords:
                clean_tokens.append(token.lemma_.lower())
    return clean_tokens

def get_signature_keyword(tweet):
    keywords_signature = []
    for token in nlp(tweet):
        token_str = token.text.lower()
        if token_str in tweeter_keywords:
            keywords_signature.append(token_str)

        lemma_str = token.lemma_.lower()
        if lemma_str in political_keywords_:
            keywords_signature.append(lemma_str)
    return keywords_signature

def extract_top_words(input_words, top_n):
    words, frequencies = np.unique(input_words, return_counts=True)
    pair_list = list(zip(words, frequencies))
    pair_list.sort(key=lambda pair: -pair[1])
    top_list = pair_list[:top_n]

    top_words = list(map(lambda pair: pair[0], top_list))
    top_frequency = list(map(lambda pair: pair[1], top_list))
    return top_words, top_frequency

def plot_top_histogram(label_signatures, label, prefix, top_n=100):
    plt.figure(figsize=(15, 20))
    
    top_words, top_frequency = extract_top_words(label_signatures, top_n)
    plt.barh(top_words, top_frequency)
    
    picture_path = os.path.join(pictures_folder, f"{prefix}_{label}.png")
    plt.savefig(picture_path)

def get_merging_list(tweets, label):
    merging_list = []
    label_signatures = []
    cleaned_words = []
    for i, tweet in enumerate(tweets):
        signature_tweet = get_person_signature(tweet)
        label_signatures += signature_tweet

        clean_tweet = get_clean_tweet_words(tweet)
        cleaned_words += clean_tweet
        keywords_signature = get_signature_keyword(tweet)
        
        merging_list.append({
            "tweet" : tweet,
            "class": label,
            "person_signature": " ".join(signature_tweet),
            "keywords_signature": " ".join(keywords_signature)
        })
        print(f"Processing the far {label}: {i}/{len(tweets)}", end='\r')
    print()
    
    plot_top_histogram(label_signatures, label, prefix='signatures')
    plot_top_histogram(cleaned_words, label, prefix='cleaned_words')
    return merging_list

def split_words_percentages(item):
    attributions_list = item['attributions_list']
    words = [word for word, _ in attributions_list]
    percentages = [percentage for _, percentage in attributions_list]

    percentages = np.array(percentages) / np.sum(percentages)
    return words, percentages.tolist()

def extract_decisive_words(item, threshold=0.40):
    words, percentages = split_words_percentages(item)
    percentages = np.array(percentages)
    words = np.array(words)

    decisive_words = words[percentages >= threshold]
    return decisive_words.tolist()

def draw_words_attribution(item, index, attribution_path, labels_name=["Left", "Right"]):
    words, percentages = split_words_percentages(item)

    plt.figure(figsize=(10, 15))
    plt.barh(words, percentages)
    plt.axvline(np.mean(percentages), color='red', linewidth=2)
    
    label_name = labels_name[item["label"]]
    plt.title(f"Decision {label_name}")
    
    picture_path = os.path.join(attribution_path, f'attributions_{index}.png')
    plt.savefig(picture_path)
    plt.close()

def draw_all_words_attributions(experiment_path):
    attributions_path = os.path.join(experiment_path, 'attributions.json')
    with open(attributions_path, 'r', encoding='utf8') as fin:
        attributions = json.load(fin)

    attribution_path = os.path.join(experiment_path, 'atributions')
    if not os.path.exists(attribution_path):
        os.makedirs(attribution_path)

    attributed_words = []
    for index, item in enumerate(attributions):
        attributed_words += extract_decisive_words(item)
        draw_words_attribution(item, index, attribution_path)
        print(f"Drawing progress {index}/{len(attributions)}", end='\r')
    
    attributed_words = list(set(attributed_words))
    with open(atributed_vocab_path, 'w', encoding='utf8') as fout:
        fout.write('\n'.join(attributed_words))

def main():
    df_train = pd.read_csv(dev_train_path)
    
    left_mask = df_train['ideology_multiclass'] == "left"
    right_mask = df_train['ideology_multiclass'] == "right"

    left_tweets = df_train.loc[left_mask, "tweet"].tolist()
    rigth_tweets = df_train.loc[right_mask, "tweet"].tolist()

    merging_list = []
    merging_list += get_merging_list(left_tweets, 'left')
    merging_list += get_merging_list(rigth_tweets, 'right')

    with open(extremes_path, "w", encoding='utf8') as fout:
        json.dump(merging_list, fout, indent=4, ensure_ascii=False)

if __name__ == "__main__":
    main()

# TODO: mining ideas:
# pronume la persoana 1 (singular sau plural)
# political lexicon: mayor, gobierno, presidente
# bigram measure
# 
# stance detection-ish:
# in favour of "capital", "justice", "monarchy", "LBGT", "inmigrantes"
# tolerancy: acceptar, oposing, etc


# posible future improvement 
# instutution names contraption - with a spanish ner (maybe pretrained)

