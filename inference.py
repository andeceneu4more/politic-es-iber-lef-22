from __init__ import pickle_load, custom_tokenzier_path
from proc import clear_tweet
from transformers import BertTokenizer

import os
import json
import pandas as pd
import pdb
import pickle
import numpy as np

test_csv_path = os.path.join('data', 'test_without_labels.csv')
vectorizer_path = os.path.join('data', 'vectorizer_best.npy')
submission_folder = os.path.join('data', 'submission')
submission_csv = os.path.join(submission_folder, "results.csv")

gender_label_names = ["female", "male"]
profession_label_names = ["journalist", "politician"]
ideology_binary_label_names = ["left", "right"]
ideology_multiclass_label_names = ["left", "moderate_left", "moderate_right", "right"]

tokenizer = BertTokenizer.from_pretrained(custom_tokenzier_path)

def get_vectorized_test():
    df = pd.read_csv(test_csv_path)
    user_ids = df['label'].unique()
    
    tokenized_list = []
    for user_id in user_ids:
        user_mask = df['label'] == user_id
        tweets = df.loc[user_mask, "tweet"].tolist()
        for tweet in tweets:
            tweet = clear_tweet(tweet)
            tokenized = tokenizer.tokenize(tweet)
            tokenized_list.append(' '.join(tokenized))

    with open(vectorizer_path, 'rb') as fin:
        vectorizer = pickle.load(fin)
    test_text_vec = vectorizer.transform(tokenized_list)
    return test_text_vec, user_ids

def run_light(exp_dir, test_text_vec, labels_name):
    model = pickle_load(exp_dir)
    
    config_path = os.path.join(exp_dir, "config.json")
    with open(config_path, 'r') as fin:
        config = json.load(fin)

    column_target = config.get("column_target", "")
    print(f"Aplying inference on {column_target}")

    labels_enc = model.predict(test_text_vec)
    #####################################
    # ONLY if you do this at chunck level
    df = pd.read_csv(test_csv_path)
    user_ids = df['label'].unique()

    df["pred"] = labels_enc

    labels = []
    for user_id in user_ids:
        user_mask = df['label'] == user_id
        pred = df.loc[user_mask, "pred"].tolist()

        bincount = np.bincount(pred)
        label_enc = np.argmax(bincount)

        labels.append(labels_name[label_enc])
    ##################################

    # labels = [labels_name[int(label_enc)] for label_enc in labels_enc]
    return labels


def main():
    test_text_vec, user_ids = get_vectorized_test()
    
    df = pd.DataFrame()
    df["user"] = user_ids
    
    df["gender"] = run_light("logs/SVC_9", test_text_vec, gender_label_names)
    df["profession"] = run_light("logs/SVC_10", test_text_vec, profession_label_names)
    df["ideology_binary"] = run_light("logs/SVC_15", test_text_vec, ideology_binary_label_names)
    df["ideology_multiclass"] = run_light("logs/SVC_8", test_text_vec, ideology_multiclass_label_names)
    
    df.to_csv(submission_csv, index=False)


def unify():
    df = pd.read_csv(test_csv_path)
    user_ids = df['label'].unique()

    new_df = pd.DataFrame()
    new_df["user"] = user_ids
    for i, label_name in enumerate(["gender", "profession", "ideology_binary", "ideology_multiclass"]):
        computed_path = os.path.join(submission_folder, f"results_{i}.csv")
        computed_df = pd.read_csv(computed_path)
        new_df[label_name] = computed_df["ideology_binary"]

    # ex_path = os.path.join(submission_folder, "results_best.csv")
    # ex_df = pd.read_csv(ex_path)
    
    # vot_rez = [min(vote_1, vote_2) for vote_1, vote_2 in zip(ex_df["profession"].tolist(), new_df["profession"].tolist())]
    # new_df["profession"] = vot_rez
    new_df.to_csv(submission_csv, index=False)

if __name__ == "__main__":
    # main()
    unify()