from transformers import BertForSequenceClassification, BertModel, BertTokenizer
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.svm import SVC
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score, confusion_matrix, f1_score
from xgboost import XGBClassifier


from __init__ import get_new_experiment_folder, train_json, dev_json, logs_folder, custom_tokenzier_path, vectorized_data_path, vectorizer_path, data_folder, pickle_load, train_chunk_json, dev_chunk_json
from importance import plot_binary_classification_importance

from scipy import sparse
from nltk.corpus import stopwords

from nltk.stem import SnowballStemmer
stemmer = SnowballStemmer("spanish")

stopwords_dict = {stop: True for stop in stopwords.words('spanish')}

import json
import pdb
import numpy as np
import os 
import pickle
import seaborn as sns
import string
import torch

np.random.seed(101)
device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu') 


tokenizer = BertTokenizer.from_pretrained("CenIA/bert-base-spanish-wwm-cased-finetuned-mldoc")
model = BertForSequenceClassification.from_pretrained("CenIA/bert-base-spanish-wwm-cased-finetuned-mldoc").to(device)
model.eval()

composed_labels_map = {
    (0, 0): 0,
    (0, 1): 1,
    (1, 0): 3,
    (1, 1): 2
}
from transformers.models.bert.modeling_bert import BertForSequenceClassification

def extract_tokenized_texts(json_path, column_target, column_input='clean_tweet', to_join=True, to_filter=False, with_embeddings=True):
    with open(json_path, 'r', encoding='utf8') as fin:
        json_list = json.load(fin)
    
    tokenized_list = []
    embeddings = []
    labels = []
    for i, item in enumerate(json_list):
        if column_input == "signature":
            tokenized_list.append(item[column_input])
        else:
            tokenized = tokenizer.tokenize(item[column_input])
            if to_filter:
                # tokenized = [stemmer.stem(token) for token in item[column_input].split(' ') if not stopwords_dict.get(token, False)]

                tokenized = list(filter(lambda token: not stopwords_dict.get(token, False) and \
                                                    not token in string.punctuation and \
                                                    not token.startswith('#'), tokenized))
            if to_join:
                tokenized_list.append(' '.join(tokenized))
            else:
                tokenized_list.append(tokenized)

        if with_embeddings:
            with torch.no_grad():
                input_dict = tokenizer(item[column_input], return_tensors='pt').to(device)
                output = model.bert(**input_dict)
                embedding = output.pooler_output[0].cpu().numpy()
                embeddings.append(embedding)

        labels.append(item[column_target])
        print(f"Tokenizing progress {i}/{len(json_list)}", end='\r')
    print()

    embeddings = sparse.csr_matrix(embeddings)
    return tokenized_list, embeddings, labels



def get_model_by_config(config):
    model_name = config.get("model_name", "")
    model_config = config.get("model_config", "")
    if model_name == "SVC":
        model = SVC(**model_config)
    elif model_name == "RandomForestClassifier":
        model = RandomForestClassifier(**model_config)
    elif model_name == "XGBClassifier":
        model = XGBClassifier(**model_config)
    return model 

def save_sk_model(model, exp_dir):
    model_path = os.path.join(exp_dir, "best.pkl")
    with open(model_path, 'wb') as fout:
        pickle.dump(model, fout)



def get_vectorized_data(column_target, column_input, selection_input, with_embeddings=False):
    vectorized_labels_path = os.path.join(data_folder, f"vectorized_{column_target}.npy")

    if os.path.exists(vectorized_data_path) and os.path.exists(vectorized_labels_path):
        train_texts_vec, dev_texts_vec = np.load(vectorized_data_path, allow_pickle=True)
        train_labels, dev_labels = np.load(vectorized_labels_path, allow_pickle=True)
    else:
        train_j = train_chunk_json if selection_input == "chunk" else train_json
        dev_j = dev_chunk_json if selection_input == "chunk" else dev_json

        tokenized_train, train_embeddings, train_labels = extract_tokenized_texts(train_j, column_target, column_input, with_embeddings)
        tokenized_dev, dev_embeddings, dev_labels = extract_tokenized_texts(dev_j, column_target, column_input, with_embeddings)

        vectorizer = TfidfVectorizer(analyzer='word', lowercase=True, max_features=10000, ngram_range=(1, 3))
        train_texts_vec = vectorizer.fit_transform(tokenized_train)
        dev_texts_vec = vectorizer.transform(tokenized_dev)

        # train_texts_vec = sparse.hstack([train_texts_vec, train_embeddings])
        # dev_texts_vec = sparse.hstack([dev_texts_vec, dev_embeddings])

        with open(vectorizer_path, "wb") as fout:
            pickle.dump(vectorizer, fout)

        vectorized_texts = np.array([train_texts_vec, dev_texts_vec])
        np.save(vectorized_data_path, vectorized_texts)

        vectorized_labels = np.array([train_labels, dev_labels])
        np.save(vectorized_labels_path, vectorized_labels)
        
    return train_texts_vec, dev_texts_vec, train_labels, dev_labels

def compute_all_metrics(dev_labels, pred_labels):
    accuracy = accuracy_score(dev_labels, pred_labels)
    f1_weighted = f1_score(dev_labels, pred_labels, average='weighted')
    f1_macro = f1_score(dev_labels, pred_labels, average='macro')
    print(f"Model finished with accuracy {accuracy}, f1 weighted {f1_weighted}, f1 macro {f1_macro}")
    return accuracy, f1_weighted, f1_macro

def plot_and_save_confusions(experiment_folder, conf_matrix):
    confusions_filename = os.path.join(experiment_folder, "confusions.png")
    sns_plot = sns.heatmap(conf_matrix, annot=True, fmt='', cmap='Blues')
    fig = sns_plot.get_figure()
    fig.savefig(confusions_filename)

def get_mixed_up(train_texts_vec, train_labels, lmbda=0.8):
    training_size = train_texts_vec.shape[0]
    random_indices = np.random.randint(0, training_size, int(0.3 * training_size))
    new_vecs = lmbda * train_texts_vec[random_indices[:-1]] + (1 - lmbda) * train_texts_vec[random_indices[1:]]

    train_labels = np.array(train_labels).astype(np.float64)
    new_labels = lmbda * train_labels[random_indices[:-1]] + (1 - lmbda) * train_labels[random_indices[1:]]
    new_labels = np.round(new_labels).astype(int)

    train_texts_vec = sparse.vstack([train_texts_vec, new_vecs])
    train_labels = np.concatenate([train_labels, new_labels])
    return train_texts_vec, train_labels


def composed_multiclass_predictions(dev_texts_vec, dev_labels):
    binary_dir = os.path.join(logs_folder, 'SVC_6')
    middle_dir = os.path.join(logs_folder, 'SVC_7')

    model_binary = pickle_load(binary_dir)
    model_middle = pickle_load(middle_dir)

    binary_pred = model_binary.predict(dev_texts_vec)
    middle_pred = model_middle.predict(dev_texts_vec)
    
    pred_labels = [composed_labels_map[pair] for pair in zip(binary_pred, middle_pred)]
    compute_all_metrics(dev_labels, pred_labels)
    
def test_composed_multiclass():
    _, dev_texts_vec, _, dev_labels = get_vectorized_data("ideology_multiclass_enc")
    composed_multiclass_predictions(dev_texts_vec, dev_labels)
    # Model finished with accuracy 0.6355733082706767, f1 weighted 0.6358016712423517, f1 macro 0.6200189499024724
    # Versus best trained alone
    # Model finished with accuracy 0.6504934210526315, f1 weighted 0.6494193030125521, f1 macro 0.6392612024638181

def main(with_training):
    config = {
        "model_name": "RandomForestClassifier",
        "model_config": {
            # 'kernel': "rbf",
            'class_weight': 'balanced',
            'verbose': 10
        },
        "description": "AFTER FIXING THE BUG: from bert encodings (version 2 with user accumulator and curriculum sorting as well). Added ngrams(1,3) and removed mixup. to_filter=True",
        "column_target": "profession_enc",
        "column_input": "ner_free_tweet", 
        "selection": "chunk",
        "with_mixup": False
    }

    column_target = config.get('column_target', '')
    column_input = config.get('column_input', '')
    selection_input = config.get('selection', '')
    train_texts_vec, dev_texts_vec, train_labels, dev_labels = get_vectorized_data(column_target, column_input, selection_input)

    if config.get("with_mixup", False):
        train_texts_vec, train_labels = get_mixed_up(train_texts_vec, train_labels)
    
    if with_training:
        model_name = config.get("model_name")
        exp_dir = get_new_experiment_folder(model_name)
        
        model = get_model_by_config(config)
        model.fit(train_texts_vec, train_labels)
        save_sk_model(model, exp_dir)

    else:
        exp_dir = os.path.join(logs_folder, 'SVC_11')
        model = pickle_load(exp_dir)
        
        config_path = os.path.join(exp_dir, "config.json")
        with open(config_path, 'r') as fin:
            config = json.load(fin)
    
    pred_labels = model.predict(dev_texts_vec)
    accuracy, f1_weighted, f1_macro = compute_all_metrics(dev_labels, pred_labels)
    config.update({
        "acccuracy": float(accuracy),
        "f1_weighted": float(f1_weighted),
        "f1_macro": float(f1_macro)
    })
    config_path = os.path.join(exp_dir, "config.json")
    with open(config_path, 'w') as fout:
        json.dump(config, fout, indent=4, sort_keys=True)
    
    conf_matrix = confusion_matrix(dev_labels, pred_labels)
    plot_and_save_confusions(exp_dir, conf_matrix)
    
    # plot_svm_binary_classification_viz(exp_dir, model)


if __name__ == "__main__":
    main(with_training=True)
    # test_composed_multiclass() 

# profession check filtered tokens
# add scipy morph tokens and check both gender and profession