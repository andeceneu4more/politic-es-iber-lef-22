from __init__ import train_json, dev_json
from train_test_light import extract_tokenized_texts, compute_all_metrics
from gensim.models.ldamodel import LdaModel
from gensim.corpora.dictionary import Dictionary
from tqdm import tqdm

import numpy as np
import pdb

column_target = 'ideology_multiclass_enc'
num_topics = 4

tokenized_train, train_labels = extract_tokenized_texts(train_json, column_target, to_join=False, to_filter=False)
tokenized_dev, dev_labels = extract_tokenized_texts(dev_json, column_target, to_join=False, to_filter=False)

tokenized_dictionary = Dictionary(tokenized_train)
train_corpus = [tokenized_dictionary.doc2bow(text) for text in tokenized_train]
dev_corpus = [tokenized_dictionary.doc2bow(text) for text in tokenized_dev]

id2token = {value: key for key, value in tokenized_dictionary.token2id.items()}

lda = LdaModel(train_corpus, num_topics=num_topics)

lda_labels = []
for text in tqdm(dev_corpus):
    probas = np.array(lda[text])[:, 1]
    label = np.argmax(probas)
    lda_labels.append(label)

compute_all_metrics(dev_labels, lda_labels)

for topic_id in range(num_topics):
    print(f"Topic number {topic_id} ended with: ")

    topic_info = lda.show_topic(topic_id)
    topic_info = [(id2token[int(word_id)], word_proba) for word_id, word_proba in topic_info]
    print(topic_info)


# ideology_binary_enc
# Model finished with accuracy 0.5099859022556391, f1 weighted 0.509550033857338, f1 macro 0.4994139569870414

# gender_enc
# Model finished with accuracy 0.534421992481203, f1 weighted 0.536372282550247, f1 macro 0.5333967411800851

# profession_enc
# Model finished with accuracy 0.48355263157894735, f1 weighted 0.5358467318379051, f1 macro 0.439593134041093

# ideology_middle_enc
# Model finished with accuracy 0.42857142857142855, f1 weighted 0.4382319119668168, f1 macro 0.416058993737069

# ideology_multiclass_enc
# 