from transformers import BertTokenizer
from models import PoliticalBeto

spanish_text = "la apuesta del principado por los pozos que todos sean parques científico-tecnológicos"
beto_tokenizer = BertTokenizer.from_pretrained('dccuchile/bert-base-spanish-wwm-uncased')
tokens = beto_tokenizer.encode(spanish_text, return_tensors='pt')


model = PoliticalBeto().cuda()
result = model(tokens.cuda())


# also to test the overflowing text
# solution: maybe a model for selecting the region of interest? - validate by asking, is this aplied in computer vision?
# or maybe a learnable attention mask?
# https://arxiv.org/pdf/2103.13597.pdf


# tests here that will be separated


# translator_model = MarianMTModel.from_pretrained('Helsinki-NLP/opus-mt-es-en')
# translator_model.cuda().eval()

# translator_tokenizer = MarianTokenizer.from_pretrained('Helsinki-NLP/opus-mt-es-en')

# spanish_tokens = translator_tokenizer.encode(spanish_text, return_tensors='pt').cuda()
# with torch.no_grad():
#     english_tokens = translator_model.generate(spanish_tokens)[0]

# english_text = translator_tokenizer.decode(english_tokens, skip_special_tokens=True)


# stance_model = RobertaModel.from_pretrained('cardiffnlp/twitter-roberta-base-stance-feminist')
# stance_model.cuda().eval()

# stance_tokenizer = RobertaTokenizer.from_pretrained('cardiffnlp/twitter-roberta-base-stance-feminist')
# english_tokens = stance_tokenizer.encode(english_text, return_tensors='pt').cuda()
# with torch.no_grad():
#     output = stance_model(english_tokens)

# pdb.set_trace()
